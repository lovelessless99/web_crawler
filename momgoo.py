import pymongo
import json
client = pymongo.MongoClient(host='localhost')

# 創建一個DB
db = client['movie_top100']

# 創建一個
collection = db['movie']
data = json.load(open('movie.json', 'r', encoding='utf-8'))
datas = [data[str(i)] for i in range(1, 101, 1)]
result = collection.insert(datas)
print(result)