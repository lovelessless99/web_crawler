import requests, re
import json


def get_one_page(url):
    headers = {
        'User-Agent': 'mozilla/5.0 (macintosh intel mac os x 10_13_3) applewebkit/537.36(KHML, like Gecko) '
                      'Chrome/65.0.3325.162 Safari/537.36',
    }

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.text
    return None


def parse_one_page(html):
    test = r"<dd>\n.*board-index.*>(.*)</i>\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*<p.*>(.*)</a></p>\n.*\n[\s]*(.*)\n.*\n.*>" \
           r"(.*)</p>.*\n.*\n.*\n.*[\s]*</div>[\s]*</div>[\s]*</dd>"
    pattern = re.compile(test)
    items = re.findall(pattern, html)

    data = []
    for item in items:
        movie = {'rank': item[0], 'name': item[1], 'actor': item[2], 'time': item[3]}
        data.append(movie)
    return data
    # json.load(open('movie.json', 'r', encoding='utf-8'))
    # json.dump(data, open('movie.json', 'w', encoding='utf-8'), ensure_ascii=False, indent=4)


if __name__ == '__main__':
    totol_data = []
    for i in range(10):
        url = 'http://maoyan.com/board/4?offset=%s' % str(i*10)
        html = get_one_page(url)
        totol_data.append(parse_one_page(html))

    data = {}
    for page in totol_data:
        for i in page:
            data[int(i['rank'])] = i

    json.dump(data, open('movie.json', 'w', encoding='utf-8'), ensure_ascii=False, indent=4)
